import discord
from discord.ext import commands
import asyncio


class Utilities(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=["serverinvite"], pass_context=True)
    # @asyncio.coroutine
    async def invite(self, ctx):
        """Provides wotbot invitation link."""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        embed = discord.Embed(
            title=_("Bot invitation link"),
            description=_(
                "Click on the invitation link to invite {} to your discord server.\nhttps://discordapp.com/oauth2/authorize?client_id={}&scope=bot&permissions=0"
            ).format(self.bot.user.name, self.bot.user.id),
            colour=234,
            type="rich",
            url="https://discordapp.com/oauth2/authorize?client_id={}&scope=bot&permissions=0".format(
                self.bot.user.id
            ),
        )
        embed.add_field(
            name=_("Privacy message:"),
            value=_(
                "WotBot respects your privacy. **It doesn't expose user messages in any way. It doesn't log user messages.** It does keep track of used commands (see the `?botstats` for popular commands). Wotbot does keep log of commands and command arguments (like discord or WG usernames or IDs) for troubleshooting purposes. User data saved via settings and platoon-connector contain Discord user ID, Wargaming ID and some preference (for example region or language). None of the saved or observed data is shared with any third party in any way. Wotbot doesn't conduct any discord administrative tasks."
            ),
        )
        embed.add_field(
            name=_("Source code:"),
            value=_(
                "Wotbot source code is open, feel free to check it at any time on [Gitlab](https://gitlab.com/vanous/wotbot/)."
            ),
        )
        embed.set_thumbnail(
            url="https://user-images.githubusercontent.com/3680926/31844711-a67cba2e-b5fa-11e7-83ef-9c9ae6e8e67d.png"
        )
        try:
            await ctx.message.author.send(content=None, embed=embed)
        except discord.Forbidden:
            self.bot.logger.warning("Please enable Embed links permission for wotbot.")
            await ctx.send(
                content=_("Please enable Embed links permission for wotbot.")
            )


def setup(bot):
    bot.add_cog(Utilities(bot))
