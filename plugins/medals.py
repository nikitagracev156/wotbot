import discord
from discord.ext import commands
import asyncio
from operator import itemgetter


class UserStats(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    # @asyncio.coroutine
    async def medals(self, ctx, *, player_name: str = None):
        """Get players medals."""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        meds = [
            "heroesOfRassenay",
            "medalLafayettePool",
            "medalKolobanov",
            "medalRadleyWalters",
            "mainGun",
        ]
        # achievements_wg=self.bot.wg.wotb_servers["eu"].encyclopedia.achievements()
        achievements_wg = await self.bot.wg.get_all_achievements()

        player, region, token, ownuser = await self.bot.wg.get_local_player(
            player_name, ctx
        )

        if not player:
            return

        self.bot.logger.info(("medals:", player_name))

        # player=wotb.account.list(type="exact",search=player_name)

        if player:
            # self.bot.logger.debug(player)
            player_id = str(player[0]["account_id"])
            player_nick = player[0]["nickname"]
            # player_achiev=self.bot.wg.wotb_servers[region].account.achievements(account_id=player_id)
            player_achiev = await self.bot.wg.get_wg(
                region=region,
                cmd_group="account",
                cmd_path="achievements",
                parameter="account_id={}".format(player_id),
            )
            player_achiev = player_achiev[player_id]
            self.bot.logger.debug(player_achiev)
            player_achiev = player_achiev.get("achievements", False)
            if not player_achiev:
                await ctx.send(_("No data."))
        for i in meds:
            if i in player_achiev.keys():
                embed = discord.Embed(
                    title=achievements_wg[i]["name"],
                    description="{}\n \uFEFF".format(achievements_wg[i]["description"]),
                    colour=234,
                    type="rich",
                )
                embed.add_field(
                    name="\uFEFF", value="```{}```".format(player_achiev[i])
                )
                embed.set_thumbnail(url=achievements_wg[i]["image"])
                # await bot.add_reaction(msg,'\U0001F44D') #test of votable emoji → "reaction"
                try:
                    await ctx.send(content=None, embed=embed)
                except discord.Forbidden:
                    self.bot.logger.warning(
                        "Please enable Embed links permission for wotbot."
                    )
                    await ctx.send(
                        content=_("Please enable Embed links permission for wotbot.")
                    )


def setup(bot):
    bot.add_cog(UserStats(bot))
