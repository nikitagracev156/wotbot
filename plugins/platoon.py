import discord
from discord.ext import commands
import asyncio
from operator import itemgetter
from tabulate import tabulate
import datetime
import re

toon_to_names = {
    "1sc": "1 Seal Clubbing",
    "2sc": "2 Seal Clubbing",
    "3sc": "3 Seal Clubbing",
    "4sc": "4 Seal Clubbing",
    "5sc": "5 Seal Clubbing",
    "1": "1",
    "2": "2",
    "3": "3",
    "4": "4",
    "5": "5",
    "6": "6",
    "7": "7",
    "8": "8",
    "9": "9",
    "10": "10",
    "1-4": "1-4",
    "5-8": "5-8",
    "8-10": "8-10",
}


class PlatoonConnector(commands.Cog):
    """Connect players across discord servers"""

    def __init__(self, bot):
        self.bot = bot

    def get_count(self, ctx):

        online = 0
        offline = 0
        idle = 0
        other = 0
        servers = 0
        if ctx.message.guild is not None:
            if str(ctx.message.guild.id) not in self.bot.ServerCfg:
                servers += 1
                for member in ctx.message.guild.members:
                    if member.status == discord.Status.online:
                        online += 1
                    elif member.status == discord.Status.offline:
                        offline += 1
                    elif member.status == discord.Status.idle:
                        idle += 1
                    else:
                        other += 1

        for server_id in self.bot.ServerCfg:
            if "platoon-channel" in self.bot.ServerCfg[server_id]:
                server = self.bot.get_guild(int(server_id))
                if server is not None:
                    servers += 1
                    for member in server.members:
                        if member.status == discord.Status.online:
                            online += 1
                        elif member.status == discord.Status.offline:
                            offline += 1
                        elif member.status == discord.Status.idle:
                            idle += 1
                        else:
                            other += 1
        return [servers, online, offline, idle, other]

    @commands.group(pass_context=True)
    # @asyncio.coroutine
    async def platoon(self, ctx):
        """List of platoon requests. Lists last 60 minutes"""
        # self.bot.logger.debug("one")
        # self.bot.logger.debug(self.bot.sqlite.version())
        if ctx.invoked_subcommand is None:
            data = await self.bot.sqlite.get_platoons(limit=60)
            self.bot.logger.debug(data)
            if len(data):
                id = data[0][0]
                join = "Contact user by sending ```@wotbot platoon join ID```for example ```@wotbot platoon join {}```".format(
                    id
                )
                toutotal = "```{}\n```{}".format(
                    tabulate(
                        data,
                        headers=["ID", "user", "tier", "region", "posted"],
                        tablefmt="grid",
                        stralign="right",
                        numalign="right",
                    ),
                    join,
                )
            else:
                toutotal = "No Player is waiting for Platoon. Post your platoon request: `?platoon post tier`, where "
                toutotal += "Tier is a number between 1 and 10, or ranges: 1-4, 5-8, 8-10, or 1-5sc for seal-clubbing."
            
            await ctx.send(toutotal)

    @platoon.command(pass_context=True)
    # @asyncio.coroutine
    async def list(self, ctx, limit: int = 60):
        """List of platoon requests. You can specify minutes to go back, default is 60"""
        self.bot.logger.info("platoon")
        data = await self.bot.sqlite.get_platoons(limit=limit)
        if len(data):
            id = data[0][0]
            join = "Contact user by sending ```@wotbot platoon join ID```for example ```@wotbot platoon join {}```".format(
                id
            )
            toutotal = "```{}\n```{}".format(
                tabulate(
                    data,
                    headers=["ID", "user", "tier", "region", "posted"],
                    tablefmt="grid",
                    stralign="right",
                    numalign="right",
                ),
                join,
            )
        else:
            toutotal = "no data"
        await ctx.send(toutotal)

    @platoon.command(pass_context=True)
    # @asyncio.coroutine
    async def post(self, ctx, tier: str = "", region: str = "eu"):
        """Post platoon request. Specify tier and optionally region, example: platoon post 7 asia"""
        self.bot.logger.info("platoon post")
        # correct_tier=re.compile('10|[1-9]$') #tiers
        correct_tier = re.compile("^[1-5]sc$|^1-4$|^5-8$|^8-10$|^10$|^[1-9]$")  # tiers
        # "^[1-9]sc$|^9r$|^9rating$|^10rating$|^10r$|^10$|^[1-9]$"
        if not correct_tier.match(tier.lower()):
            await ctx.send(
                "Tier is a number between 1 and 10, or ranges: 1-4, 5-8, 8-10, or 1-5sc for seal-clubbing."
            )
            return
        correct_region = re.compile("(eu|ru|na|asia)$")  # tiers
        if not correct_region.match(region):
            await ctx.send("Allowed regions are: eu, ru, na, asia.")
            return
        if tier not in toon_to_names:
            await ctx.send(
                "Tier is a number between 1 and 10, or ranges: 1-4, 5-8, 8-10, or 1-5sc for seal-clubbing."
            )
            return
        if True:
            # if not self.bot.sqlite.check_platoon(ctx.message.author.id):
            platoon_id = await self.bot.sqlite.add_platoon(
                ctx.message, toon_to_names[tier], region.lower()
            )
            count = self.get_count(ctx)
            idle = ""
            other = ""
            if count[3] > 0:
                idle = ", {} idle".format(count[3])
            if count[4] > 0:
                other = ", {} other".format(count[4])

            await ctx.send(
                "Added to queue. Will be seen in {0[0]} servers by {0[1]} online{1}{2} members.".format(
                    count, idle, other
                )
            )
            for server in self.bot.ServerCfg:
                if "platoon-channel" in self.bot.ServerCfg[server]:
                    channel_id = self.bot.ServerCfg[server].get("platoon-channel", None)
                    if channel_id is not None:
                        if channel_id != str(
                            ctx.message.channel.id
                        ):  # do not send to origin channel
                            if discord.Object(id=channel_id) is not None:
                                try:
                                    await discord.Object(id=channel_id).send(
                                        content="`{}` placed tier {} platoon request in the {} queue, ID={}. Type ?platoon to view it, or ```?platoon join {}``` to join it.".format(
                                            ctx.message.author.name,
                                            toon_to_names[tier],
                                            region,
                                            platoon_id,
                                            platoon_id,
                                        )
                                    )
                                except:
                                    self.bot.logger.info(
                                        "platoon sending to non existing room"
                                    )

        else:
            await ctx.send("You can only send one platoon per 20 minutes.")

    @platoon.command(pass_context=True)
    # @asyncio.coroutine
    async def join(self, ctx, p_id: int = None):
        """Reply to platoon request. Specify ID, example platoon join 1"""
        if p_id is None:
            await ctx.send("use ID from the list of platoons")
        else:
            data = await self.bot.sqlite.get_platoon(p_id)
            if data:
                # self.bot.logger.debug(("data: ",data["id"], data["user_id"]))
                invite = await self.bot.get_channel(
                    id=379728855239360515
                ).create_invite(
                    temporary=1, max_uses=4, max_age=600
                )  # 379728855239360515 general channel in wot-bot platoons
                # await ctx.send(invite)dd
                await ctx.send(
                    "Excellent. Contacting both platoon mates. Check Direct message from @wotbot and follow the link to meet your fellow tanker."
                )
                await self.bot.get_user(id=int(data["user_id"])).send(
                    content="Hello tanker, here is a link to meet your platoon mate. It is valid for next 20 minutes."
                )  # original poster
                await self.bot.get_user(id=int(data["user_id"])).send(
                    content=invite
                )  # original poster
                await ctx.message.author.send(
                    content="Hello tanker, here is a link to meet your platoon mate. It is valid for next 20 minutes."
                )  # responder
                await ctx.message.author.send(content=invite)  # responder
                await self.bot.sqlite.join_platoon(p_id)
            else:
                await ctx.send("Platoon request already taken.")


def setup(bot):
    bot.add_cog(PlatoonConnector(bot))
