from pathlib import Path
import difflib
import xml.etree.ElementTree
from pprint import pprint
import sys
from random import randint
import json
import csv

# param=sys.argv[1]
class Data:
    def __init__(self):
        p = Path("../game_data/latest/XML/item_defs/vehicles/")
        files = list(p.glob("**/*.xml"))
        # self.w=[word.name.split(".")[0].replace("_"," ")  for word in files]
        # self.full={word.name.split(".")[0].replace("_"," "):word  for word in files}
        # self.full_list=[word.parts[-1] for word in files]
        self.full_list = {word.parts[-1].replace(".xml", ""): word for word in files}
        # pprint(self.full_list)

        # with open('/home/petr/bin/wotbot/DATA/4.6/xmltankdata.json') as bs_data:
        #    bs_json = json.load(bs_data)

        self.w = []
        self.full = {}
        self.small_to_big = {}
        self.name_map = {}
        with open("../game_data/latest/en.yaml") as wg_strings:
            reader = csv.reader(
                wg_strings, delimiter=":", quotechar='"', skipinitialspace=True
            )
            for row in reader:
                if row[0].startswith("#"):
                    row[0].replace('"#', "")
                    portionsL = row[0].split(":")
                    if len(portionsL) > 1:
                        portionsR = row[1].split(":")
                        if len(portionsR) > 1:
                            right = portionsR[1]
                        else:
                            right = row[1]

                        self.name_map[portionsL[1]] = right
                        # if "lupus" in portionsL[1].lower():
                        #    print("trying",portionsL[1])
                        if portionsL[1] in self.full_list:
                            # print(row,right)
                            self.w.append(right.lower())
                            self.small_to_big[right.lower()] = right
                            self.full[right] = self.full_list[portionsL[1]]

        # pprint(self.full_list)
        # print(self.name_map)

        # self.w=[]
        # self.full={}
        # self.small_to_big={}
        # for bs_tank in bs_json:
        #    if bs_tank.get("names",None) is not None:
        #        #print(bs_tank["names"]["en"])
        #        if bs_tank.get("fileName", None) is not None:
        #            if bs_tank["fileName"].split('/')[-1] in self.full_list:
        #                self.small_to_big[bs_tank["names"]["en"].lower()]=bs_tank["names"]["en"]
        #                self.w.append(bs_tank["names"]["en"].lower())
        #                self.full[bs_tank["names"]["en"]]=bs_tank["fileName"].replace("/Users/dave/Dev/tank-compare/xmlstuff/vehicles/", "/home/petr/bin/wotbot/DATA/4.6/XML/item_defs/vehicles/")

    def shell_details(self, shell):
        for i in self.guns_shells.iter(shell):
            # print(i)
            return i.find("kind").text, i.find("damage").find("armor").text

    def get_shells(self, gun):
        if gun not in self.guns:
            self.guns[gun] = {}
            self.guns[gun]["ammo"] = {}

        for i in self.guns_guns.iter(gun):
            # print(i)
            for shell in i.iter("shots"):
                self.guns[gun]["pitchLimits"] = i.find("pitchLimits").text
                pitch = self.get_pitchlimits(gun)
                print(pitch)
                if pitch is not None:
                    self.guns[gun]["pitchLimits"] = pitch

                for shot in shell.getchildren():
                    self.guns[gun]["ammo"][shot.tag] = {}
                    self.guns[gun]["ammo"][shot.tag]["penetration"] = shot.find(
                        "piercingPower"
                    ).text.split()[0]
                    self.guns[gun]["tier"] = i.find("level").text
                    # self.guns[gun]["reload"]=i.find("reloadTime").text
                    self.guns[gun]["reload"] = self.get_reload(gun)

                    kind, damage = self.shell_details(shot.tag)
                    self.guns[gun]["ammo"][shot.tag]["name"] = kind
                    self.guns[gun]["ammo"][shot.tag]["damage"] = damage

    def get_guns(self):
        gun_list = []
        # tank = xml.etree.ElementTree.parse(self.full[self.res[0]].as_posix()).getroot()
        # self.tank_data = xml.etree.ElementTree.parse(self.full[self.small_to_big[self.res[0]]]).getroot()
        self.tank_data = xml.etree.ElementTree.parse(
            self.full[self.small_to_big[self.res[0]]].as_posix()
        ).getroot()
        for guns in self.tank_data.iter("guns"):
            gun_list += [gun.tag for gun in guns.getchildren()]
        for i in set(gun_list):
            self.get_shells(i)

    def get_tier(self):
        # for item in self.tank_info.iter(self.full[self.small_to_big[self.res[0]]].split('/')[-1].replace(".xml","")):
        for item in self.tank_info.iter(
            self.full[self.small_to_big[self.res[0]]].parts[-1].replace(".xml", "")
        ):
            return item.find("level").text

    def get_reload(self, gun):
        for item in self.tank_data.iter(gun):
            return item.find("reloadTime").text

    def get_pitchlimits(self, gun):
        # print("looking for pitch",xml.etree.ElementTree.tostring(self.tank_data))
        # print(gun)
        for item in self.tank_data.iter(gun):
            # print(xml.etree.ElementTree.tostring(item))
            # print(item.find("pitchLimits").text)
            if item.find("pitchLimits") is not None:
                # print(item.find("pitchLimits").text)
                return item.find("pitchLimits").text
            else:
                # print("nic")
                return None

    def hledej(self, param):
        factor = 0.6
        while True:
            self.res = difflib.get_close_matches(param.lower(), self.w, 3, factor)
            print("Found:", self.res)
            print("Factor:", factor)
            if not self.res:
                factor = factor - 0.1
            else:
                break

        if not self.res:
            return None

        result = self.res[0]
        # country=self.full[self.res[0]].parts[-2]
        country = self.full[self.small_to_big[self.res[0]]].parts[-2]
        # country=self.full[self.small_to_big[self.res[0]]].split('/')[-2]
        self.guns_shells = xml.etree.ElementTree.parse(
            "../game_data/latest/XML/item_defs/vehicles/"
            + country
            + "/components/shells.xml"
        ).getroot()
        self.guns_guns = xml.etree.ElementTree.parse(
            "../game_data/latest/XML/item_defs/vehicles/"
            + country
            + "/components/guns.xml"
        ).getroot()
        self.tank_info = xml.etree.ElementTree.parse(
            "../game_data/latest/XML/item_defs/vehicles/" + country + "/list.xml"
        ).getroot()
        self.guns = {}
        self.get_guns()

        # apply strings from translations
        for gun in self.guns:
            # print("apply translation:", gun)
            new_name = self.name_map.get(gun, None)
            if new_name is not None:
                if new_name not in self.guns:
                    print("old name, new name:", gun, new_name)
                    self.guns[new_name] = self.guns.pop(gun)

        # out[result]={}
        # out[result]["name"]=self.res[0]
        # out[result]["guns"]=self.guns
        out = {}
        out["name"] = self.small_to_big[self.res[0]]
        out["tier"] = self.get_tier()
        out["guns"] = self.guns
        return out

    def do_all(self):
        all = {}
        for tank in self.w:
            if tank not in [
                "list",
                "customization",
                "chassis",
                "engines",
                "fuelTanks",
                "guns",
                "radios",
                "shells",
                "turrets",
                "vehicle",
                "vehicle_effects",
                "stuff_packages",
                "stuff",
                "shot_effects",
                "provisions",
                "player_emblems",
                "optional_devices",
                "optional_device_slots",
                "horns",
                "gun_effects",
                "damage_stickers",
                "consumables",
                "chassis_effects",
                "camouflages",
            ]:
                all[tank] = self.hledej(tank)
        with open("../game_data/latest/gamedata-new.json", "w") as outfile:
            outfile.write(json.dumps(all))


if __name__ == "__main__":
    c = Data()
    c.do_all()
