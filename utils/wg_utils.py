from operator import itemgetter
import re

from collections import OrderedDict
import aiohttp

import os
from pathlib import Path
import json
import datetime
import collections
import re
import inspect


class WG:
    def __init__(self, token, bot):
        self.wg_token = token
        self.bot = bot

        self.wotb_servers = ["eu", "ru", "na", "asia"]

    async def get_wg(
        self,
        region,
        cmd_group,
        cmd_path,
        parameter,
        language=None,
        wgn=None,
        access_token=None,
    ):
        if access_token is None:
            token = self.wg_token
        else:
            token = self.bot.cfg["WargamingPrivateToken"]

        if language is None:
            language = "en"

        base_path = "https://api.wotblitz.{realm}/wotb/{cmd_group}/{cmd_path}/?application_id={token}&language={language}&{parameter}"
        if wgn is not None:
            base_path = "https://api.worldoftanks.{realm}/wgn/{cmd_group}/{cmd_path}/?application_id={token}&language={language}&{parameter}"

        wg_regions = {"eu": "eu", "ru": "ru", "na": "com", "asia": "asia"}

        url = base_path.format(
            realm=wg_regions[region],
            token=token,
            cmd_group=cmd_group,
            cmd_path=cmd_path,
            language=language,
            parameter=parameter,
        )

        async with aiohttp.ClientSession(loop=self.bot.loop) as client:
            try:
                for i in range(3):
                    async with client.get(url) as r:
                        if r.status != 200:
                            return
                        res = await r.json()
                        if res.get("status") == "ok":
                            return res.get("data")
                        elif res.get("status") == "error":
                            if res["error"]["code"] == 504:
                                print("retry on err 504", i)
                            else:
                                print(res["error"]["message"])
                                return

            except Exception as e:
                print("Wargaming connection error", e)
                self.bot.logger.warning("WG connection error", e)
                return None

    async def get_rating(self, player_id, region, ctx, score=0):
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext

        rating_region = {
            "eu": ["eu", "en"],
            "na": ["com", "en"],
            "ru": ["ru", "ru"],
            "asia": ["asia", "en"],
        }
        league_icon = {
            0: "https://static-wbp-eu.wgcdn.co/dcont/1.29.0/fb/image/a1.png",
            1: "https://static-wbp-eu.wgcdn.co/dcont/1.29.0/fb/image/league_big.png",
            2: "https://static-wbp-eu.wgcdn.co/dcont/1.29.0/fb/image/b1.png",
            3: "https://static-wbp-eu.wgcdn.co/dcont/1.29.0/fb/image/d1.png",
            4: "https://static-wbp-eu.wgcdn.co/dcont/1.29.0/fb/image/c1.png",
            5: "",
        }

        league_name = {
            5: _("Platinum"),
            4: _("Bronze"),
            3: _("Silver"),
            2: _("Gold"),
            1: _("Platinum"),
            0: _("Diamond"),
        }

        url = "https://wotblitz.{}/{}/api/rating-leaderboards/user/{}".format(
            rating_region[region][0], rating_region[region][1], player_id
        )
        async with aiohttp.ClientSession(loop=self.bot.loop) as client:
            async with client.get(url) as r:
                player_rating = "Response code: {}".format(r.status)
                player_rating_league_id = 5
                player_rating_league = ""
                player_rating_league_icon = None
                if r.status == 200:
                    try:
                        player_rating_raw = await r.json()
                    except:
                        player_rating = "Response code: WG Server not available"
                        print("rating not available URL: ", url)
                        player_rating_raw=None
                    self.bot.logger.debug(player_rating_raw)
                    if player_rating_raw:
                        player_rating = _("Calibration Battles left: {}").format(
                            player_rating_raw["calibrationBattlesLeft"]
                        )
                        rating_data = player_rating_raw.get("neighbors", None)
                        if rating_data:
                            for rat_data in rating_data:
                                self.bot.logger.debug("DATA: ", rat_data)
                                if str(rat_data.get("spa_id", 0)) == str(player_id):
                                    player_rating = _("Points: {}, Place: {}").format(
                                        rat_data.get("score", 0),
                                        rat_data.get("number", 0),
                                    )
                                    if score == 1:
                                        player_rating = rat_data.get("score", 0)
                                    if score == 2:
                                        player_rating = rat_data.get("score", 0)
                                        player_rating_place = rat_data.get("number", 0)
                                        player_rating_league = league_name[
                                            rat_data.get("league_index", 0)
                                        ]
                                        return (
                                            player_rating,
                                            player_rating_place,
                                            player_rating_league,
                                        )
                                    else:
                                        player_rating_league = _(
                                            'League: "{}", '
                                        ).format(
                                            league_name[rat_data.get("league_index", 0)]
                                        )
                                    player_rating_league_icon = league_icon[
                                        rat_data.get("league_index", 0)
                                    ]

        return (player_rating, player_rating_league_icon, player_rating_league)

    async def get_all_averages(self):
        difference = 60 * 60 * 12  # one day
        my_file_name = "./game_data/all_averages.json"
        my_file = Path(my_file_name)
        if my_file.is_file():
            st = os.stat(my_file_name)
            mtime = st.st_mtime
            if (
                datetime.datetime.now() - datetime.datetime.fromtimestamp(mtime)
            ).total_seconds() < difference:
                with open(my_file_name) as averages:
                    all_averages = json.load(averages)
                return all_averages

        url = "https://www.blitzstars.com/api/tankaverages.json"
        async with aiohttp.ClientSession(loop=self.bot.loop) as client:
            async with client.get(url) as r:

                if r.status == 200:
                    bs_averages = await r.json()

                    averages_all = {}

                    for tank in bs_averages:
                        l = tank.get("all", None)
                        averages_all[tank["tank_id"]] = {
                            "spots": l.get("spotted", 0),
                            "wins": l.get("wins", 0),
                            "dmg": l.get("damage_dealt", 0),
                            "kills": l.get("frags", 0),
                            "battles": l.get("battles", 1),
                            "dcp": l.get("dropped_capture_points", 0),
                            "avg_wr": l.get("wins", 0) / l.get("battles", 1) * 100,
                            "avg_dmg": l.get("damage_dealt", 0) / l.get("battles", 1),
                            "avg_spots": l.get("spotted", 0) / l.get("battles", 1),
                            "avg_kills": l.get("frags", 0) / l.get("battles", 1),
                            "avg_dcp": l.get("dropped_capture_points", 0)
                            / l.get("battles", 1),
                        }

                    with open(my_file_name, "w+") as averages:
                        json.dump(averages_all, averages)

                    return averages_all

    async def get_all_achievements(self):
        difference = 60 * 60 * 24  # one day
        my_file_name = "./game_data/all_achievements.json"
        my_file = Path(my_file_name)
        if my_file.is_file():
            st = os.stat(my_file_name)
            mtime = st.st_mtime
            if (
                datetime.datetime.now() - datetime.datetime.fromtimestamp(mtime)
            ).total_seconds() < difference:
                with open(my_file_name) as achievements:
                    all_achievements = json.load(achievements)
                return all_achievements

        all_achievements = await self.get_wg(
            region="eu", cmd_group="encyclopedia", cmd_path="achievements", parameter=""
        )
        all_achievements = dict(all_achievements)
        # clear cache to see changes!

        with open(my_file_name, "w+") as achievements:
            json.dump(all_achievements, achievements)
        return all_achievements

    async def get_all_vehicles(self):

        my_file_name = "./game_data/tanks.json"
        my_file = Path(my_file_name)
        if my_file.is_file():
            with open(my_file_name) as vehicles:
                all_vehicles = json.load(vehicles)
                if all_vehicles["data"].get("385",None) is None:
                    all_vehicles["data"]["385"]= { "tank_id": 385, "nation": "italy", "tier": 10, "is_premium": "false", "type": "mediumTank", "name": "Progetto M40 mod. 65" }
            return all_vehicles["data"]

    async def get_all_vehicles_old(self):

        difference = 60 * 60 * 24  # one day
        my_file_name = "./game_data/all_vehicles.json"
        my_file = Path(my_file_name)
        if my_file.is_file():
            st = os.stat(my_file_name)
            mtime = st.st_mtime
            if (
                datetime.datetime.now() - datetime.datetime.fromtimestamp(mtime)
            ).total_seconds() < difference:
                with open(my_file_name) as vehicles:
                    all_vehicles = json.load(vehicles)
                return all_vehicles

        url = "https://wotinspector.com/static/armorinspector/tank_db_blitz.js"
        async with aiohttp.ClientSession(loop=self.bot.loop) as client:
            async with client.get(url) as r:

                if r.status == 200:
                    all_vehicles = await r.text()
                all_vehicles = all_vehicles.replace("TANK_DB =", "")[:-4] + "}"

                all_vehicles = re.sub(r"(\d+)(:)", r'"\1":', all_vehicles)
                all_vehicles = re.sub(r"(\"en\")(:)", '"name":', all_vehicles)
                print(all_vehicles)
                all_vehicles = json.loads(all_vehicles)
                all_vehicles = dict(all_vehicles)

                with open(my_file_name, "w+") as vehicles:
                    json.dump(all_vehicles, vehicles)
                return all_vehicles

    def get_servers(self, region=None):
        if region in self.wotb_servers:
            c = self.wotb_servers.copy()
            c.insert(0, c.pop(c.index(region)))
            # c.move_to_end(region, last=False)
            return c
        else:
            return self.wotb_servers

    async def get_local_name_by_id(self, id):  # need this for join command
        if str(id) in self.bot.PlayerCfg:
            confirmedaccount_id = self.bot.PlayerCfg[str(id)].get(
                "confirmedaccount_id", None
            )
            # if confirmedaccount_id is not None and confirmedaccount_id != "":
            if confirmedaccount_id:
                playerregion = self.bot.PlayerCfg[str(id)].get("confirmedregion", None)
                userdata = await self.bot.sqlite.get_user(confirmedaccount_id)
                self.bot.logger.info(userdata)

                if userdata is not None:
                    if userdata != []:
                        playername = userdata[3]
                        return "{}@{}".format(playername, playerregion)

            playername = self.bot.PlayerCfg[str(id)].get("playername", None)
            if playername is not None:
                return playername

            return None
        else:
            return None

    async def get_local_name(self, ctx):
        user = ctx.message.author
        if ctx.message.mentions:
            print("mentions")
            user = ctx.message.mentions[0]
            if self.bot.user == user:
                if len(ctx.message.mentions) > 1:
                    user = ctx.message.mentions[1]
                else:
                    user = ctx.message.author

        if str(user.id) in self.bot.PlayerCfg:
            confirmedaccount_id = self.bot.PlayerCfg[str(user.id)].get(
                "confirmedaccount_id", None
            )
            ownuser = False
            self.bot.logger.info(confirmedaccount_id)

            # if confirmedaccount_id is not None and confirmedaccount_id != "":
            if confirmedaccount_id:
                playerregion = self.bot.PlayerCfg[str(user.id)].get(
                    "confirmedregion", None
                )
                # playeraccount_id = self.bot.PlayerCfg[str(user.id)].get(
                #    "confirmedaccount_id", None
                # )
                # if confirmed:
                userdata = await self.bot.sqlite.get_user(confirmedaccount_id)
                self.bot.logger.info(userdata)

                if ctx.message.author == user:
                    ownuser = True
                if userdata is not None:
                    if userdata != []:
                        playername = userdata[3]
                        return (*userdata, playername, ownuser)

                else:
                    pass

            playername = self.bot.PlayerCfg[str(user.id)].get("playername", None)
            if playername is not None and playername != "":
                return playername

            return user.name
        else:
            return user.name

    async def get_local_player(self, player_name, ctx):
        token = None
        ownuser = False
        player = None
        region = None
        if player_name is None or ctx.message.mentions:
            player_name = await self.get_local_name(ctx)
        self.bot.logger.info(player_name)

        if isinstance(player_name, tuple):
            token = player_name[2]
            player_id = str(player_name[0])
            # player = [{"nickname": player_name[3], "account_id": player_name[0]}]
            region = player_name[1]
            ownuser = player_name[5]
            player, region = await self.get_player_by_id_a(player_id)
            player = [player[player_id]]
        else:
            player, region = await self.get_player_a(player_name, ctx.message)

        if not player:
            out = await self.bot.wg.search_player_a(ctx, player_name)
            await self.bot.dc.not_found_msg(ctx, out)
        return player, region, token, ownuser

    async def get_players_count(self):
        total = {"Region": [], "Players": []}

        for srv in self.wotb_servers:
            reg = await self.get_wg(
                region="{}".format(srv),
                cmd_group="servers",
                cmd_path="info",
                parameter="game=wotb",
                wgn=True,
            )
            if reg.get("wotb"):
                total["Region"].append(reg["wotb"][0]["server"])
                total["Players"].append(reg["wotb"][0]["players_online"])

        return total

    async def get_player_a(self, player_name, msg):
        player = False
        region = None
        player_name = player_name.lower()
        self.bot.logger.debug(("get", player_name))
        correct = re.compile(".{3}.*@(eu|ru|na|asia)$")  # user @ region
        if "@" in player_name:
            # change to regexp later
            if correct.match(player_name):
                player_name, region = player_name.lower().split("@")
                if len(player_name) < 3:
                    return player, region
                player = await self.get_wg(
                    region=region,
                    cmd_group="account",
                    cmd_path="list",
                    parameter="type=exact&search={}".format(player_name),
                )
                print("got player:", player)
        else:
            self.bot.logger.debug(("ttt", player_name))
            clan_match = re.compile("(.*)\[.*\].*")  # name[clan]
            if clan_match.match(player_name):  # is name[clan]
                player_name = clan_match.match(player_name).group(1)
            cfg_region = None
            if msg.guild and str(msg.guild.id) in self.bot.ServerCfg:
                cfg_region = self.bot.ServerCfg[str(msg.guild.id)].get("region", None)
            if len(player_name) < 3:
                return player, region
            for region in self.get_servers(cfg_region):
                self.bot.logger.debug(("trying region:", region))
                player = await self.get_wg(
                    region=region,
                    cmd_group="account",
                    cmd_path="list",
                    parameter="type=exact&search={}".format(player_name),
                )
                print("got player:", player)
                if player:
                    break
        try:
            await self.bot.sqlite.add_user(
                player[0]["account_id"], player[0]["nickname"], region
            )
            # print("adding user to database")
        except Exception as e:
            self.bot.logger.error(e)
        return player, region

    async def get_players_by_ids_a(self, player_id, region=None):
        player = False
        if region:
            self.bot.logger.debug(("trying region:", region))
            player = await self.get_wg(
                region=region,
                cmd_group="account",
                cmd_path="info",
                parameter="account_id={}".format(player_id),
            )
            self.bot.logger.debug(player)
            if list(player.values())[0]:
                return player, region

        for region in self.get_servers():
            self.bot.logger.debug(("trying region:", region))
            player = await self.get_wg(
                region=region,
                cmd_group="account",
                cmd_path="info",
                parameter="account_id={}".format(player_id),
            )

            self.bot.logger.debug(player)
            if list(player.values())[0]:
                return player, region
        return player, region

    async def get_player_by_id_a(self, player_id):
        player = False
        region = self.get_region(int(player_id))
        player = await self.get_wg(
            region=region,
            cmd_group="account",
            cmd_path="info",
            parameter="account_id={}".format(player_id),
        )
        if not player[player_id]:
            return False, region

        try:
            await self.bot.sqlite.add_user(
                player[player_id]["account_id"], player[player_id]["nickname"], region
            )
        except Exception as e:
            self.bot.logger.error(e)
        return player, region

    def get_region(self, id):
        if id < 500000000:
            return "ru"
        if id < 1000000000:
            return "eu"
        if id < 2000000000:
            return "na"
        return "asia"

    async def search_player_a(self, ctx, player_name):
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        player_name = player_name.lower()
        correct = re.compile(".{3}.*@(eu|ru|na|asia)$")  # user @ region
        out = _(
            "Didn't find player `{}`, try correct spelling. You can always set your WG player name via ?conf or via WotBot Dashboard: https://wotbot.pythonanywhere.com/ . {}"
        )
        found_text = _("This is what i found @{}:{}")
        out_list = ""
        if "@" in player_name:
            if not correct.match(player_name):
                return out.format(player_name, "")
            player_name, region = player_name.lower().split("@")
            player_search = await self.get_wg(
                region=region,
                cmd_group="account",
                cmd_path="list",
                parameter="type=startswith&search={}".format(player_name),
            )
            if player_search:
                for i in player_search[:20]:
                    out_list += "%s\n" % i["nickname"]
                return out.format(
                    player_name,
                    found_text.format(region, "```md\n{}```".format(out_list)),
                )
        else:
            for region in self.get_servers():
                player_search = await self.get_wg(
                    region=region,
                    cmd_group="account",
                    cmd_path="list",
                    parameter="type=startswith&search={}".format(player_name),
                )
                if player_search:
                    for i in player_search[:20]:
                        out_list += "%s\n" % i["nickname"]
                    return out.format(
                        player_name,
                        found_text.format(region, "```md\n{}```".format(out_list)),
                    )
        return out.format(player_name, "")

    def wn8_color(self, wn8):
        wn8 = int(wn8)
        if wn8 >= 2900:
            return 0x5A3175
        elif wn8 >= 2350:
            return 0x83579D
        elif wn8 >= 1900:
            return 0x4A92B7
        elif wn8 >= 1600:
            return 0x4C762E
        elif wn8 >= 1250:
            return 0x6D9521
        elif wn8 >= 900:
            return 0xD7B600
        elif wn8 >= 600:
            return 0xD77900
        elif wn8 >= 300:
            return 0xCD3333
        else:
            return 0x000000

        return 0x000000

    def winrate_color(self, wr):
        wr = int(wr)
        if wr > 65:
            return 0x5A3175
        elif wr > 60:
            return 0x83579D
        elif wr > 56:
            return 0x4A92B7
        elif wr > 54:
            return 0x4C762E
        elif wr > 52:
            return 0x6D9521
        elif wr > 49:
            return 0xD7B600
        elif wr > 47:
            return 0xD77900
        elif wr > 45:
            return 0xCD3333
        else:
            return 0x000000

        return 0x000000

    async def get_clan_a(self, clan_name):
        correct = re.compile(".{1}.*@(eu|ru|na|asia)$")  # user @ region
        if "@" in clan_name:
            if correct.match(clan_name):
                clan_name, region = clan_name.lower().split("@")
                clan_list = await self.get_wg(
                    region=region,
                    cmd_group="clans",
                    cmd_path="list",
                    parameter="search={}&fields=clan_id,tag,name".format(clan_name),
                )
            else:
                return None, None
        else:
            for region in self.get_servers():
                clan_list = await self.get_wg(
                    region=region,
                    cmd_group="clans",
                    cmd_path="list",
                    parameter="search={}&fields=clan_id,tag,name".format(clan_name),
                )
                # self.bot.logger.debug((len(clan_list), clan_list, region))
                if clan_list:
                    return clan_list, region
        return clan_list, region
