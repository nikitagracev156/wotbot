#!/bin/bash
source data/crowdin.config
echo "generating report"
hash=$(curl -F "format=csv" https://api.crowdin.com/api/project/WotBot/reports/top-members/export?key=$key | sed -n 's/.*<hash>\([^}]*\)<\/hash>/\1/p')
#echo "hs:" $hash
wget -O top_members.csv "https://api.crowdin.com/api/project/WotBot/reports/top-members/download?key=$key&hash=$hash"
