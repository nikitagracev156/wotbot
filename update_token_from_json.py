from pathlib import Path
import glob
import aiosqlite as lite
import asyncio
import json

data = [(0, 0)]


async def db():
    async with lite.connect(
        "users.db", detect_types=lite.core.sqlite3.PARSE_DECLTYPES
    ) as db:
        for i in data:
            print(i, i[0], i[1])
            await db.execute(
                "update users set token = $token where account_id=$user",
                {"token": i[1], "user": i[0]},
            )
        await db.commit()


async def saved():
    for i in data:
        # for p in Path("stats/daily/").glob("*{}*".format(i)):
        for p in glob.glob("stats/**/*{}*".format(i), recursive=True):
            Path(p).unlink()
            print(i, p)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(db())
    # loop.run_until_complete(saved())
